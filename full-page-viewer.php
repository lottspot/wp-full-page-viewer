<?php

/**
 * Plugin Name: Full Page Viewer
 * Description: Add a "view all pages" link to paginated posts
 * Version: 1.0
 * Author: James Lott
 * Author URI: www.lottspot.com
 * License: Gnu General Publi License v3
 */

class FullPageViewer {

	protected $page_class = 'page';
	protected $placement = 'before';
	protected $default_placement = 'before';
	protected $query_var = 'viewall';

	public function __construct() {
	  add_filter( 'wp_link_pages_args', array($this, 'render_viewall_link'), 11 );
	  add_filter( 'the_content', array($this, 'render_all_pages' ) );
	}

	public function render_all_pages( $content ) {
	  
	  if ($_GET[$this->query_var]) {
	    if ( is_single() && $GLOBALS['multipage'] ) {
	      $content = "<div class=\"$this->page_class\">" . join("</div><div class=\"$this->page_class\">", $GLOBALS['pages']) . '</div>';
	    }
	  }
	  return $content;
	}

	public function render_viewall_link( $args ) {
	  if ( is_single() ) {
	    if ( $this->placement == 'before' ) {
	      $args[$this->placement] = sprintf( 
		'%s%s', 
		$args[$this->placement],
		$this->get_viewall_link($args)
	      );
	    } elseif ( $this->placement == 'after' ) {
	      $args[$this->placement] = sprintf( 
		'%s%s', 
		$this->get_viewall_link($args),
		$args[$this->placement]
	      );
	    } else {
	      $this->placement = $default_placement;
	      return $this->render_viewall_link( $args );
	    }
	  }
	  return $args;
	}

	private function get_viewall_link( $args ) {
	  $link = NULL;
	  if(!$_GET[$this->query_var]) { 
	    $link = sprintf( 
	      '<a href="%s">%sView all%s</a>', 
	      add_query_arg(array($this->query_var => 1)),
	      $args['link_before'],
	      $args['link_after']
	    );
	  } else {
	    $link = sprintf( 
	      '<a href="%s">%sBack to selected page%s</a>', 
	      remove_query_arg($this->query_var),
	      $args['link_before'],
	      $args['link_after']
	    );
	  }	
	  return $link;
	}

}

new FullPageViewer()

?>
